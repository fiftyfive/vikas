package com.darksky.poc.util;

import org.junit.Assert;
import org.junit.Test;


public class AppUtilTest {
    @Test
    public void testGetFormattedDateOfInputZone() {
        String result = AppUtil.getFormattedDate("Asia/Kolkata", (int)(System.currentTimeMillis()
                / 1000));
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetFormattedDateOfInputZoneNull() {
        String result = AppUtil.getFormattedDate(null, (int)(System.currentTimeMillis()
                / 1000));
        Assert.assertNull(result);
    }

    @Test
    public void testGetFormattedShortStringTimeOfInputZone() {
        String result = AppUtil.getFormattedDate("Asia/Kolkata", (int)(System.currentTimeMillis()
                / 1000));
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetFormattedShortStringTimeOfInputZoneNull() {
        String result = AppUtil.getFormattedShortStringTime(null, (int)(System.currentTimeMillis()
                / 1000));
        Assert.assertNull(result);
    }

    @Test
    public void testGetFormattedTimeOfInputZone() {
        String result = AppUtil.getFormattedTime("Asia/Kolkata");
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetFormattedTimeOfInputZoneNull() {
        String result = AppUtil.getFormattedTime(null);
        Assert.assertNull(result);
    }
}
