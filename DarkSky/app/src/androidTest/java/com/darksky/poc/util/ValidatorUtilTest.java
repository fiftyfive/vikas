package com.darksky.poc.util;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorUtilTest {
    @Test
    public void testIsValidLatitudeInputWrongData() {
        String latitude = "abc";
        Assert.assertEquals(ValidatorUtil.isValidLatitude(latitude), false);
    }

    @Test
    public void testIsValidLatitudeInputNullData() {
        String latitude = null;
        Assert.assertEquals(ValidatorUtil.isValidLatitude(latitude), false);
    }

    @Test
    public void testIsValidLatitudeInputCorrectData() {
        String latitude = "0.0";
        Assert.assertEquals(ValidatorUtil.isValidLatitude(latitude), true);
    }

    @Test
    public void testIsValidLongitudeInputWrongData() {
        String longitude = "abc";
        Assert.assertEquals(ValidatorUtil.isValidLongitude(longitude), false);
    }

    @Test
    public void testIsValidLongitudeInputNullData() {
        String longitude = null;
        Assert.assertEquals(ValidatorUtil.isValidLongitude(longitude), false);
    }

    @Test
    public void testIsValidLongitudeInputCorrectData() {
        String longitude = "0.0";
        Assert.assertEquals(ValidatorUtil.isValidLongitude(longitude), true);
    }
}
