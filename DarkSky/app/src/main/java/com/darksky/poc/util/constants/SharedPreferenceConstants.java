package com.darksky.poc.util.constants;

/**
 * This interface defines constants related to Shared Preference persistent medium.
 */
public interface SharedPreferenceConstants {
    // App Shared Preference Name
    String PREFERENCE_NAME = "com.darksky.poc.APP_PREFERENCE";
    // Shared PReference Keys
    String KEY_LOCATION_DATA_LONGITUDE = "com.darksky.poc.KEY_LOCATION_DATA_LONGITUDE";
    String KEY_LOCATION_DATA_LATITUDE = "com.darksky.poc.KEY_LOCATION_DATA_LATITUDE";
}
