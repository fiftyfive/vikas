package com.darksky.poc.fragements;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.darksky.poc.R;
import com.darksky.poc.model.LocationData;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.model.view.LocationDataViewModel;
import com.darksky.poc.util.AppUtil;
import com.darksky.poc.util.constants.AppBundleKeyConstants;
import com.darksky.poc.util.constants.CommonConstants;
import com.darksky.poc.util.constants.ForecastIconType;
import com.darksky.poc.views.SkyconView;

import java.util.Timer;
import java.util.TimerTask;


/**
 * This is a fragment class which is responsible to display user interface for current whether
 * of a particular time zone.
 */
public class WeatherCurrentForecastFragment extends BaseFragment implements
        View.OnClickListener {
    // Properties Declarations
    // Views Object Declarations
    private ProgressBar mProgress;
    private RelativeLayout mSkyIconContainerLayout;
    private LinearLayout mContainerLayout;
    private TextView mCurrentTemperatureTxtView;
    private TextView mTimeZoneTxtView;
    private TextView mSummaryTxtView;
    private TextView mTimeTextView;
    private TextView mSummaryBigTextView;
    private TextView mTimeZoneMessageTextView;
    private TextView mForecastActionMessageView;
    private Button mTemperatureHourlyBtn;
    private RelativeLayout mInfoContainer;
    private SkyconView mSkyconView;
    // Callback Instance - WeatherHoursForecastClickCallback
    private WeatherHoursForecastClickCallback mWeatherHoursForecastClickListener;
    // Timer Task
    private Timer mUpdateZoneTimer;
    private UpdateZoneTimeTimerTask mUpdateZoneTimerTask;

    /**
     *
     * This method returns instance of WeatherCurrentForecastFragment.
     *
     * @param latitude : double
     * @param longitude : double
     *
     * @return WeatherCurrentForecastFragment
     */
    public static WeatherCurrentForecastFragment newInstance(double latitude,
                                                 double longitude) {
        WeatherCurrentForecastFragment fragment = new WeatherCurrentForecastFragment();
        Bundle args = new Bundle();
        args.putDouble(AppBundleKeyConstants.BUNDLE_KEY_LATITUDE, latitude);
        args.putDouble(AppBundleKeyConstants.BUNDLE_KEY_LONGITUDE, longitude);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * This class is responsible to update current time details.
     */
    private class UpdateZoneTimeTimerTask extends TimerTask {
        private String zone;

        UpdateZoneTimeTimerTask(String zone) {
            this.zone = zone;
        }

        @Override
        public void run() {
            if(getActivity() != null && getActivity().getMainLooper() != null ) {
                Handler handler = new Handler(getActivity().getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mTimeTextView.setText(AppUtil.getFormattedTime(zone));
                    }
                });
            } else {
                cancelTimers();
            }
        }
    }

    /**
     * Callback Interface : This interface needs to be implemented to do the action when
     * user clicked on Hourly Weather Forecast Link.
     */
    public interface WeatherHoursForecastClickCallback {
        void onWeatherHoursForecastClicked();
    }

    /**
     * This method set callback instance.
     *
     * @param weatherHoursForecastClickCallback - WeatherHoursForecastClickCallback
     */
    public void setWeatherHoursForecastClickCallback
            (WeatherHoursForecastClickCallback weatherHoursForecastClickCallback) {
        this.mWeatherHoursForecastClickListener = weatherHoursForecastClickCallback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weather_forecast, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Initialization of views
        init(view);
    }

    /**
     * This method initialized fragment views
     *
     * @param view : Fragment View
     */
    private void init(View view) {
        // Set View Properties Instances from initialized views
        mProgress = view.findViewById(R.id.progressBar);
        mSkyIconContainerLayout = view.findViewById(R.id.sky_icon_container);
        mContainerLayout = view.findViewById(R.id.container);
        mCurrentTemperatureTxtView = view.findViewById(R.id.tv_temperature);
        mTimeZoneTxtView = view.findViewById(R.id.tv_timezone);
        mSummaryTxtView = view.findViewById(R.id.tv_summary);
        mSummaryBigTextView = view.findViewById(R.id.tv_big_summary);
        mTimeZoneMessageTextView = view.findViewById(R.id.tv_message);
        mForecastActionMessageView = view.findViewById(R.id.tv_action_message);
        mTimeTextView = view.findViewById(R.id.tv_time);
        mTemperatureHourlyBtn = view.findViewById(R.id.btn_temperature_hourly);
        mInfoContainer = view.findViewById(R.id.info_container);
        // Hide Views
        mContainerLayout.setVisibility(View.INVISIBLE);
        mTemperatureHourlyBtn.setVisibility(View.GONE);
        mTimeZoneMessageTextView.setVisibility(View.GONE);
        mForecastActionMessageView.setVisibility(View.GONE);
        // Register Listeners
        wireListeners();
        // Fetch Weather Forecast Response from API.
        fetchWeatherForecastResponse();
    }

    /**
     * This method registers listeners for events.
     */
    private void wireListeners() {
        mTemperatureHourlyBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_temperature_hourly) {
            if(mWeatherHoursForecastClickListener != null) {
                mWeatherHoursForecastClickListener.onWeatherHoursForecastClicked();
            }
        }
    }

    /**
     * This method request to fetch Weather Forecast Response from API.
     */
    private void fetchWeatherForecastResponse() {
        if(AppUtil.isNetworkAvailable(this.getContext())) {
            Bundle bundle = this.getArguments();
            // Get Activity Registered View Model.
            LocationDataViewModel viewModel = ViewModelProviders.of(getActivity()).
                    get(LocationDataViewModel.class);
            // Added Observer to listen Forecast Response update.
            viewModel.getForecastResponseLiveData().observe(this.getActivity(), new Observer<Object>() {
                @Override
                public void onChanged(@Nullable Object o) {
                    handleForecastResponse(o);
                }
            });
            LocationData locationData;
            // If location data has been provided in bundle instance.
            if(bundle != null) {
                double latitude = bundle.getDouble(AppBundleKeyConstants.BUNDLE_KEY_LATITUDE);
                double longitude = bundle.getDouble(AppBundleKeyConstants.BUNDLE_KEY_LONGITUDE);
                locationData = new LocationData();
                locationData.setLatitude(latitude);
                locationData.setLongitude(longitude);
                viewModel.fetchForecastResponseForLocationData(this.getContext(), locationData);
            } else {
                if (getActivity() != null) {
                    // Invoke API if currently existing valid live data.
                    if (viewModel.getLocationLiveData() != null &&
                            viewModel.getLocationLiveData().getValue() != null) {
                        mProgress.setVisibility(View.VISIBLE);
                        locationData = viewModel.getLocationLiveData().getValue();
                        viewModel.fetchForecastResponseForLocationData(this.getContext(), locationData);
                    }
                }
            }
        } else {
            mProgress.setVisibility(View.GONE);
            showError(R.string.label_dialog_title, R.string.label_dialog_network_unavailable_error);
        }
    }

    /**
     * This method handle API response.
     *
     * @param obj - VolleyError | DarkSkyForecastResponse
     */
    private void handleForecastResponse(Object obj) {
        mProgress.setVisibility(View.GONE);
        // obj is type of VolleyError
        if(obj instanceof VolleyError) {
            VolleyError error = (VolleyError) obj;
            showError(getString(R.string.label_dialog_title), error.getMessage());
        } else if (obj instanceof DarkSkyForecastResponse) {
            // obj is type of DarkSkyForecastResponse
            DarkSkyForecastResponse response = (DarkSkyForecastResponse) obj;
            // Update User Interface.
            updateUserInterface(response);
        } else {
            showError(R.string.label_dialog_title, R.string.label_dialog_data_error);
        }
    }

    /**
     * This method update fragment user interface.
     *
     * @param response : DarkSkyForecastResponse
     */
    private void updateUserInterface(final DarkSkyForecastResponse response) {
        // Preventive null check before updating ui.
        if(response != null && response.getCurrently() != null) {
            mContainerLayout.setVisibility(View.VISIBLE);
            mTemperatureHourlyBtn.setVisibility(View.VISIBLE);
            // update time zone.
            mTimeZoneTxtView.setText(response.getTimezone());
            // update temperature.
            mCurrentTemperatureTxtView.setText(response.getCurrently().getTemperature().toString()
                    + (char) 0x00B0 + getString(R.string.label_graph_temperature_append_c));
            // update summary
            mSummaryTxtView.setText(String.format(getString(R.string.label_weather_condition),
                    response.getCurrently().getSummary()));
            mSummaryBigTextView.setText(response.getDaily().getSummary());
            mForecastActionMessageView.setVisibility(View.VISIBLE);
            mTimeZoneMessageTextView.setVisibility(View.VISIBLE);
            String icon = response.getCurrently().getIcon();
            if(icon != null) {
                addSkyconViewIntoLayout(icon);
            }
            String zone = response.getTimezone();
            if(zone != null) {
                startZoneTimeUpdateTask(zone);
            }
        } else {
            showError(R.string.label_dialog_title, R.string.label_dialog_current_weather_data_error);
        }
    }

    /**
     * This method adds Skycon View which display current weather conditions in UI.
     */
    private void addSkyconViewIntoLayout(String icon) {
        // Create Skycon View according to currently response of icon type.
        mSkyconView =
                AppUtil.getSkyconView(this.getActivity(), icon);
        // Preventive Null Check
        if (mSkyconView != null) {
            // set layout params for view.
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    mSkyIconContainerLayout.getLayoutParams();
            params.width = mSkyIconContainerLayout.getWidth() / 2;
            params.height = mSkyIconContainerLayout.getWidth() / 2;
            mSkyconView.setLayoutParams(params);
            /*mSkyconView.setBackground(ContextCompat.getDrawable(this.getContext(),
                    R.drawable.round_corner_gray));*/
            // update layout params for current weather basic info layout.
            params = new LinearLayout.LayoutParams(
                    mSkyIconContainerLayout.getWidth(), mSkyIconContainerLayout.getWidth() / 2);
            params.weight = 1.0f;
            params.gravity = Gravity.CENTER_VERTICAL;
            mInfoContainer.setLayoutParams(params);
            // Add Skycon View into the layout
            mSkyIconContainerLayout.addView(mSkyconView, 0);
            //setSkyIconContainerViewColor(response.getCurrently().getIcon());
        }

    }

    /**
     * This method schedule to update time on user interface for a particular zone.
     *
     * @param zone : String
     */
    private synchronized void startZoneTimeUpdateTask(final String zone) {
        cancelTimers();
        if(mUpdateZoneTimer == null) {
            mUpdateZoneTimer = new Timer();
        }
        if(mUpdateZoneTimerTask == null) {
            mUpdateZoneTimerTask = new UpdateZoneTimeTimerTask(zone);
        }
        mUpdateZoneTimer.scheduleAtFixedRate(mUpdateZoneTimerTask,
                CommonConstants.TIME_INTERVAL_ZERO, CommonConstants.TIME_INTERVAL_ONE_SECOND);
    }

    /**
     * This method cancel timers and task if running.
     */
    private void cancelTimers() {
        if(mUpdateZoneTimerTask != null) {
            mUpdateZoneTimerTask.cancel();
            mUpdateZoneTimerTask = null;
        }
        if(mUpdateZoneTimer != null) {
            mUpdateZoneTimer.cancel();
            mUpdateZoneTimer = null;
        }
    }

    private void setSkyIconContainerViewColor(String iconType) {
        switch (iconType) {
            case ForecastIconType.CLEAR_DAY:
                mContainerLayout.setBackgroundResource(R.drawable.weather_clear_day);
                break;
            case ForecastIconType.CLEAR_NIGHT:
                mContainerLayout.setBackgroundResource(R.drawable.weather_clear_night);
                break;
            case ForecastIconType.CLOUDY:
                mContainerLayout.setBackgroundResource(R.drawable.weather_cloud);
                break;
            case ForecastIconType.FOG:
                mContainerLayout.setBackgroundResource(R.drawable.weather_fog);
                break;
            case ForecastIconType.PARTIALLY_CLOUDY_DAY:
                mContainerLayout.setBackgroundResource(R.drawable.weather_partially_cloudy_day);
                break;
            case ForecastIconType.PARTIALLY_CLOUDY_NIGHT:
                mContainerLayout.setBackgroundResource(R.drawable.weather_partially_cloudy_night);
                break;
            case ForecastIconType.RAIN:
                mContainerLayout.setBackgroundResource(R.drawable.weather_rain);
                break;
            case ForecastIconType.SLEET:
                mContainerLayout.setBackgroundResource(R.drawable.weather_sleet);
                break;
            case ForecastIconType.SNOW:
                mContainerLayout.setBackgroundResource(R.drawable.weather_snow);
                break;
            case ForecastIconType.WIND:
                mContainerLayout.setBackgroundResource(R.drawable.weather_wind);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cancelTimers();
    }
}
