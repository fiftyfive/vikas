package com.darksky.poc.fragements;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This is Base Class which should be extends by others fragment in the app.  It defines
 * some utility fragment that can be used in others fragment.
 */
public class BaseFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * This method returns color value from android resource.
     *
     * @param resID - Resource ID
     *
     * @return Color (int value)
     */
    protected int getColorFromResource(int resID) {
        int color = 0;
        Context context = this.getContext();
        if(context != null) {
            color = ContextCompat.getColor(context, resID);
        }
        return color;
    }

    /**
     * This method returns dimension value from android resource.
     *
     * @param resID - Resource ID
     *
     * @return dimension (int value)
     */
    protected float getDimensionFromResource(int resID) {
        float dimen = 0.0f;
        Context context = this.getContext();
        if(context != null) {
            Resources resources = this.getContext().getResources();
            if (resources != null) {
                dimen = resources.getDimension(resID);
            }
        }
        return dimen;
    }

    /**
     * This method displays error dialog where no action is required.
     *
     * @param title - dialog title
     * @param message - message which need to be displayed
     * @param buttonText - dialog button text
     */
    protected void showError(String title, String message, String buttonText) {
        InfoDialogFragment.newInstance(title, message, buttonText).show
                                                (getFragmentManager(), null);
    }

    /**
     * This method displays error dialog where no action is required.
     *
     * @param title - dialog title
     * @param message - message which need to be displayed
     */
    protected void showError(String title, String message) {
        InfoDialogFragment.newInstance(title, message, null).show
                (getFragmentManager(), null);
    }

    /**
     * This method displays error dialog where no action is required.
     *
     * @param titleRes - title Resource
     * @param messageRes - message string resource id
     */
    protected void showError(int titleRes, int messageRes) {
        showError(getString(titleRes), getString(messageRes));
    }

    /**
     * This method displays error dialog where no action is required.
     *
     * @param titleRes - title Resource
     * @param messageRes - message string resource id
     * @param buttonTextRes - buttonTextRes id
     */
    protected void showError(int titleRes, int messageRes, int buttonTextRes) {
        showError(getString(titleRes), getString(messageRes), getString(buttonTextRes));
    }
}
