package com.darksky.poc.network;

import android.content.Context;

import com.android.volley.Response;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.network.request.DarkSkyForecastRequest;
import com.darksky.poc.util.constants.NetworkConstants;

/**
 * This is a single entry point class to hit network related api queries.
 */
public class DarkSkyAPIManager {
    /**
     * This method execute request to get current forecast response
     * of input location (Longitude / Latitude).
     *
     * @param context : App Context
     * @param listener :  Response Listener
     * @param errorListener : Error Listener
     * @param latitude : Location data - latitude
     * @param longitude : Location data - longitude
     */
    public static void getDarkSkyForeCastResponse(Context context, Response.Listener listener,
                                                  Response.ErrorListener errorListener,
                                                  double latitude,
                                                  double longitude) {
        DarkSkyForecastRequest request = new DarkSkyForecastRequest
                (NetworkConstants.DARK_SKY_BASE_URL,
                        DarkSkyForecastResponse.class,
                        listener, errorListener, latitude, longitude);
        VolleyWrapper.getInstance(context.getApplicationContext()).addToRequestQueue(request);
    }
}

