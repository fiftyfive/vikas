package com.darksky.poc.util.constants;

/**
 * This interface defines common constants of application.
 */
public interface CommonConstants {
    String TIME_FORMAT = "HH:mm:ss aa";
    String SHORT_TIME_FORMAT = "HH:mm";
    String SHORT_DATE_FORMAT = "MMM dd";

    int TIME_INTERVAL_ZERO = 0;
    int TIME_INTERVAL_ONE_SECOND = 1000;
    long TIME_ONE_HOUR_IN_MILLS = 1000L;
}
