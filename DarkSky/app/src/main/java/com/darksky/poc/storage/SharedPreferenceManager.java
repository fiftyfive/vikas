package com.darksky.poc.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.darksky.poc.model.LocationData;
import com.darksky.poc.util.constants.SharedPreferenceConstants;

/**
 * This is a singleton class to access app shared preference.
 */
public class SharedPreferenceManager {
    private static final String TAG = "SharedPreferenceManager";
    // singleton instance - lazy loading
    private static SharedPreferenceManager sInstance;
    // App Shared Preference Instance
    private SharedPreferences mSharedPreferences;

    /**
     * This method returns singleton instance of SharedPreferenceManager
     *
     * @param context : Context instance.
     *
     * @return - SharedPreferenceManager
     */
    public static SharedPreferenceManager getInstance(Context context) {
        // sInstance == null then create singleton instance (Lazy Loading)
        if(sInstance == null) {
            // Acquire Lock
            synchronized (SharedPreferenceManager.class) {
                if(sInstance == null) {
                    // sInstance == null then initialize instance.
                    sInstance = new SharedPreferenceManager(context);
                }
            }
        }
        return sInstance;
    }

    /**
     * Object Constructor.
     *
     * @param context : Context Instance
     */
    private SharedPreferenceManager(Context context) {
        // initialize preference instance.
        mSharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * This method returns instance of Location Data from Preference if exists
     * otherwise returns null.
     *
     * @return LocationData
     */
    public LocationData getLocationData() {
        LocationData data = null;
        if(mSharedPreferences != null) {
            // Get Location related from Shared Preference
            String latitudeString =
                    mSharedPreferences.getString(SharedPreferenceConstants.KEY_LOCATION_DATA_LATITUDE, "");
            String longitudeString =
                    mSharedPreferences.getString(SharedPreferenceConstants.KEY_LOCATION_DATA_LONGITUDE, "");
            if((! TextUtils.isEmpty(latitudeString)) && (! TextUtils.isEmpty(longitudeString))) {
                try {
                    double latitude = Double.parseDouble(latitudeString);
                    double longitude = Double.parseDouble(longitudeString);
                    // initialize LocationData instance.
                    data = new LocationData();
                    // set properties
                    data.setLatitude(latitude);
                    data.setLongitude(longitude);
                } catch (NumberFormatException exception) {
                    Log.e(TAG, exception.getMessage(), exception);
                }
            }
        }
        return data;
    }

    /**
     * This method save LocationData instance properties into preference.
     *
     * @param locationData : instance of LocationData
     * @return : boolean value
     */
    public boolean saveLocationData(LocationData locationData) {
        boolean result = false;
        if(mSharedPreferences != null) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(SharedPreferenceConstants.KEY_LOCATION_DATA_LATITUDE,
                    Double.toString(locationData.getLatitude()));
            editor.putString(SharedPreferenceConstants.KEY_LOCATION_DATA_LONGITUDE,
                    Double.toString(locationData.getLongitude()));
            result = editor.commit();
        }
        return result;
    }
}
