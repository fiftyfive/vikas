package com.darksky.poc.network;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Generic Volley Request Class which supports GET Protocol.
 *
 * @param <O>
 */
public class VolleyRequest<O> extends Request<O> {
    private final Gson gson = new Gson();
    private final Class<O> classType;
    private final Response.Listener<O> listener;
    protected Map<String, String> mRequestParams;

    /**
     *  Make a GET request and return a parsed object from JSON.
     *
     * @param url - URL of the request to make
     * @param classType - Relevant class object, for API Response GSON Parsing
     * @param listener - Response Listener
     * @param errorListener - Error Listener
     */
    public VolleyRequest(String url, Class<O> classType,
                         Response.Listener<O> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.classType = classType;
        this.listener = listener;
    }

    @Override
    protected void deliverResponse(O response) {
        listener.onResponse(response);
    }

    @Override
    public String getUrl() {
        return super.getUrl();
    }

    @Override
    protected Map<String, String> getParams() {
        return mRequestParams;
    }

    @Override
    protected Response<O> parseNetworkResponse(NetworkResponse response) {
        try {
            // Get Response Json from Network Response.
            String json = new String(
                    response.data, HttpHeaderParser.parseCharset(response.headers));
            // Parse Json and return response instance.
            return Response.success(
                    gson.fromJson(json, classType), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
