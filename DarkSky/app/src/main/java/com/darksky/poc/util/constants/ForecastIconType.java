package com.darksky.poc.util.constants;

/**
 * This interface defines the icons contents which comes in API response. This constant used by
 * application to update ui according to weather conditions.
 */
public interface ForecastIconType {
    String CLEAR_DAY = "clear-day";
    String CLEAR_NIGHT ="clear-night";
    String PARTIALLY_CLOUDY_DAY ="partly-cloudy-day";
    String PARTIALLY_CLOUDY_NIGHT ="partly-cloudy-night";
    String CLOUDY = "cloudy";
    String RAIN = "rain";
    String SLEET = "sleet";
    String SNOW = "snow";
    String WIND = "wind";
    String FOG = "fog";
}
