package com.darksky.poc.model;

/**
 * This is a model class which should be used in application for location data.
 */
public class LocationData  {
    private double mLatitude;
    private double mLongitude;

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }
}
