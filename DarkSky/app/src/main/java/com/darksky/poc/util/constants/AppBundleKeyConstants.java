package com.darksky.poc.util.constants;

/**
 * This interface defines constants related to the android bundle key which should be used to pass
 * data inside the application.
 */
public interface AppBundleKeyConstants {
    String BUNDLE_KEY_LATITUDE = "bundle.key.latitude";
    String BUNDLE_KEY_LONGITUDE = "bundle.key.longitude";
    String BUNDLE_KEY_DIALOG_TITLE = "bundle.key.dialog.title";
    String BUNDLE_KEY_DIALOG_MESSAGE = "bundle.key.dialog.message";
    String BUNDLE_KEY_DIALOG_INFO_BUTTON_TEXT = "bundle.key.dialog.info_button";
}
