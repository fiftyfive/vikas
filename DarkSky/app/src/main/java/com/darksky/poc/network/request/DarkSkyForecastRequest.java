package com.darksky.poc.network.request;

import com.android.volley.Response;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.network.VolleyRequest;
import com.darksky.poc.util.constants.NetworkConstants;

import java.util.HashMap;

/**
 * This class is Volley Request class which should be used to fetch Forecast Response of
 * a specific zone.
 */
public class DarkSkyForecastRequest extends VolleyRequest<DarkSkyForecastResponse> {
    /**
     * Constructor
     *
     * @param url : URL of the request to make
     * @param classType : Relevant class object, for API Response GSON Parsing
     * @param listener : Response Listener
     * @param errorListener : Error Listener
     * @param latitude : double value latitude
     * @param longitude : double value longitude
     */
    public DarkSkyForecastRequest(String url, Class classType,
                                  Response.Listener listener,
                                  Response.ErrorListener errorListener,
                                  double latitude,
                                  double longitude) {
        super(url, classType, listener, errorListener);
        // initialize map instance.
        mRequestParams = new HashMap<>();
        // Put key-value pairs.
        mRequestParams.put(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_LATITUDE,
                Double.toString(latitude));
        mRequestParams.put(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_LONGITUDE,
                Double.toString(longitude));
        mRequestParams.put(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_LONGITUDE,
                Double.toString(longitude));
        mRequestParams.put(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_UNITS,
                NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_UNITS_VALUE);
    }

    @Override
    public String getUrl() {
        return super.getUrl() +
                NetworkConstants.DARK_SKY_API_PATH_SEPARATOR +
                NetworkConstants.DARK_SKY_SERVICE_FORECAST +
                NetworkConstants.DARK_SKY_API_PATH_SEPARATOR +
                NetworkConstants.DARK_SKY_API_KEY +
                NetworkConstants.DARK_SKY_API_PATH_SEPARATOR +
                mRequestParams.get(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_LATITUDE) +
                NetworkConstants.DARK_SKY_API_COMMA_SEPARATOR +
                mRequestParams.get(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_LONGITUDE) +
                NetworkConstants.DARK_SKY_API_QUESTION_MARK +
                NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_UNITS +
                NetworkConstants.DARK_SKY_API_EQUALS +
                mRequestParams.get(NetworkConstants.DARK_SKY_SERVICE_FORECAST_PARAM_UNITS);
    }
}
