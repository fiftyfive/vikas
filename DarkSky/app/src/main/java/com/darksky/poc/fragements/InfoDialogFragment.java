package com.darksky.poc.fragements;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

import com.darksky.poc.R;
import com.darksky.poc.util.constants.AppBundleKeyConstants;

public class InfoDialogFragment extends DialogFragment {
    /**
     * This method create instance of Dialog and returns.
     *
     * @param title - Dialog Title
     * @param infoButtonText - Dialog Button Text
     * @param message - Dialog Message
     *
     * @return InfoDialogFragment
     */
    public static InfoDialogFragment newInstance(String title,
                                                 String message,
                                                 String infoButtonText) {
        InfoDialogFragment fragment = new InfoDialogFragment();
        Bundle args = new Bundle();
        args.putString(AppBundleKeyConstants.BUNDLE_KEY_DIALOG_TITLE, title);
        args.putString(AppBundleKeyConstants.BUNDLE_KEY_DIALOG_MESSAGE, message);
        args.putString(AppBundleKeyConstants.BUNDLE_KEY_DIALOG_INFO_BUTTON_TEXT, infoButtonText);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if(arguments != null) {
            // Get Title From Bundle
            String title = arguments.getString(AppBundleKeyConstants.BUNDLE_KEY_DIALOG_TITLE);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            // set dialog title
            alertDialogBuilder.setTitle(title);
            // set dialog message
            alertDialogBuilder.setMessage
                    (arguments.getString(AppBundleKeyConstants.BUNDLE_KEY_DIALOG_MESSAGE));
            // Get Button Text if provided
            String buttonText = arguments.getString
                    (AppBundleKeyConstants.BUNDLE_KEY_DIALOG_INFO_BUTTON_TEXT);
            // Set Button Listener
            alertDialogBuilder.setPositiveButton
                    (TextUtils.isEmpty(buttonText) ?
                                    getString(R.string.label_dialog_button_ok) : buttonText,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
            return alertDialogBuilder.create();
        }
        return null;
    }
}