package com.darksky.poc.fragements;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.darksky.poc.R;
import com.darksky.poc.model.LocationData;
import com.darksky.poc.model.view.LocationDataViewModel;
import com.darksky.poc.util.ValidatorUtil;

/**
 * This is a fragment class which provide user interface for location data input handle.
 */
public class LocationUserInputFragment extends BaseFragment implements View.OnClickListener,
        View.OnFocusChangeListener {
    // Properties Declarations
    // Views Object Declarations
    private Button mSubmitBtn;
    private TextInputLayout mLatitudeInputLayout;
    private TextInputLayout mLongitudeInputLayout;
    private EditText mLatitudeEditText;
    private EditText mLongitudeEditText;
    // Non Static Properties
    private boolean mIsValidLatitude;
    private boolean mIsValidLongitude;
    // Callback Instance
    private LocationSubmitCallback mLocationSubmitCallback;

    /**
     * Callback Interface : This interface needs to be implemented to do the action when
     * user successfully entered Location Data and press submit for next action.
     */
    public interface LocationSubmitCallback {
        void onLocationSubmitEvent(LocationData locationData);
    }

    /**
     * This method set callback instance.
     *
     * @param mLocationSubmitCallback : LocationSubmitCallback
     */
    public void setLocationSubmitCallback(LocationSubmitCallback mLocationSubmitCallback) {
        this.mLocationSubmitCallback = mLocationSubmitCallback;
    }

    /**
     * TextWatcher instance to listen Latitude Input Data
     */
    private TextWatcher mLatitudeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Nothing to Implement
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Validation Check fo Latitude value.
            mIsValidLatitude = ValidatorUtil.isValidLatitude(charSequence.toString());
            // If validation success then hide error otherwise display.
            if(mIsValidLatitude) {
                mLatitudeInputLayout.setError(null);
            } else {
                mLatitudeInputLayout.setError(getText(R.string.error_invalid_latitude));
            }
            // Invoke method to enable/disable submit button
            enableSubmitButton();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // Nothing to Implement
        }
    };

    /**
     * TextWatcher instance to listen Longitude Input Data
     */
    private TextWatcher mLongitudeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Nothing to Implement
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Validation Check fo Longitude value.
            mIsValidLongitude = ValidatorUtil.isValidLongitude(charSequence.toString());
            // If validation success then hide error otherwise display.
            if(mIsValidLongitude) {
                mLongitudeInputLayout.setError(null);
            } else {
                mLongitudeInputLayout.setError(getText(R.string.error_invalid_longitude));
            }
            // Invoke method to enable/disable submit button
            enableSubmitButton();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // Nothing to Implement
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_location_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Initialization of views
        init(view);
    }

    /**
     * This method initialized fragment views
     * @param view : Fragment View
     */
    private void init(View view) {
        // Set View Properties Instance from initialized views
        mSubmitBtn = view.findViewById(R.id.btn_submit);
        mLatitudeInputLayout = view.findViewById(R.id.ti_latitude);
        mLatitudeEditText = view.findViewById(R.id.et_latitude);
        mLongitudeInputLayout = view.findViewById(R.id.ti_longitude);
        mLongitudeEditText = view.findViewById(R.id.et_longitude);
        // By Default Disable Submit Button
        mSubmitBtn.setEnabled(false);
        // Register Listeners
        wireListeners();
    }

    /**
     * This method registers listeners for events.
     */
    private void wireListeners() {
        mSubmitBtn.setOnClickListener(this);
        mLatitudeEditText.addTextChangedListener(mLatitudeTextWatcher);
        mLatitudeEditText.setOnFocusChangeListener(this);
        mLongitudeEditText.addTextChangedListener(mLongitudeTextWatcher);
        mLongitudeEditText.setOnFocusChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_submit) {
            // Get Location Data (Latitude & Longitude)
            String longitudeString = mLongitudeEditText.getText().toString();
            String latitudeString = mLatitudeEditText.getText().toString();
            // If valid Location Data (Latitude & Longitude)
            if(ValidatorUtil.isValidLatitude(latitudeString)
                    && ValidatorUtil.isValidLongitude(longitudeString)) {
                // covert string into double values for latitude and longitude
                double longitude = Double.parseDouble(longitudeString);
                double latitude = Double.parseDouble(latitudeString);
                // Initialized LocationData instance.
                LocationData locationData = new LocationData();
                // Set properties
                locationData.setLongitude(longitude);
                locationData.setLatitude(latitude);
                // save location data
                saveLocationDataIntoSharedPreference(locationData);
                mLocationSubmitCallback.onLocationSubmitEvent(locationData);
            }
        }
    }

    /**
     * This method persist Location Data into Shared Preference.
     *
     * @param locationData : LocationData
     */
    private void saveLocationDataIntoSharedPreference(LocationData locationData) {
        LocationDataViewModel viewModel = ViewModelProviders.of(getActivity()).
                get(LocationDataViewModel.class);
        viewModel.saveLocationDataIntoSharedPreference(this.getContext(), locationData);
    }

    /**
     * This method enable submit button if location data (latitude & longitude) both are valid.
     */
    private void enableSubmitButton() {
        mSubmitBtn.setEnabled(mIsValidLatitude && mIsValidLongitude);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        // Handle event while leaving Focus
        if(! hasFocus) {
            // For - view (Latitude)
            if (view.getId() == R.id.et_latitude) {
                String data = mLatitudeEditText.getText().toString();
                // If empty data then set error null
                if(TextUtils.isEmpty(data)) {
                    mLatitudeInputLayout.setError(null);
                } else {
                    mIsValidLatitude = ValidatorUtil.isValidLatitude(data);
                    // If Latitude data is not valid then display error.
                    if (! mIsValidLatitude) {
                        mLatitudeInputLayout.setError(getString(R.string.error_invalid_latitude));
                    }
                }
            } else if (view.getId() == R.id.et_longitude) {
                // For - view (Longitude)
                String data = mLongitudeEditText.getText().toString();
                // If empty data then set error null
                if(TextUtils.isEmpty(data)) {
                    mLongitudeInputLayout.setError(null);
                } else {
                    mIsValidLongitude = ValidatorUtil.isValidLatitude(data);
                    // If Longitude data is not valid then display error.
                    if (! mIsValidLongitude) {
                        mLongitudeInputLayout.setError(getString(R.string.error_invalid_longitude));
                    }
                }
            }
        }
    }
}
