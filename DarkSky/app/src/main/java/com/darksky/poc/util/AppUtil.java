package com.darksky.poc.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.darksky.poc.R;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.util.constants.CommonConstants;
import com.darksky.poc.util.constants.ForecastIconType;
import com.darksky.poc.views.CloudFogView;
import com.darksky.poc.views.CloudMoonView;
import com.darksky.poc.views.CloudRainView;
import com.darksky.poc.views.CloudSnowView;
import com.darksky.poc.views.CloudSunView;
import com.darksky.poc.views.CloudView;
import com.darksky.poc.views.MoonView;
import com.darksky.poc.views.SkyconView;
import com.darksky.poc.views.SunView;
import com.darksky.poc.views.WindView;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * This class is basic utility class for application.
 */
public class AppUtil {
    private static final String TAG = "AppUtil";

    /**
     * This method read raw text from the file inside Android APK and returns string.
     *
     * @param ctx : App Context
     * @param resId : Resource ID of Raw
     * @return : string content
     */
    private static String readRawTextFile(Context ctx, int resId)
    {
        // Initialize Variables
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferReader = null;
        String line;
        StringBuilder text = new StringBuilder();
        try {
            inputStream = ctx.getResources().openRawResource(resId);
            inputReader = new InputStreamReader(inputStream);
            bufferReader = new BufferedReader(inputReader);
            // read file text line by line buffer mechanism
            while (( line = bufferReader.readLine()) != null) {
                text.append(line);
            }
        } catch (IOException exception) {
            Log.e(TAG, "Exception Occurred while reading content", exception);
            return null;
        } finally {
            // release the stream objects
            try {
                if (bufferReader != null) {
                    bufferReader.close();
                }
            } catch (IOException exception) {
                Log.e(TAG, "Exception Occurred while closing stream", exception);
            }
            try {
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException exception) {
                Log.e(TAG, "Exception Occurred while closing stream", exception);
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException exception) {
                Log.e(TAG, "Exception Occurred while closing stream", exception);
            }
        }
        return text.toString();
    }

    /**
     * This method return dummy local DarkSkyForecastResponse instance from raw json api response.
     *
     * @param context - Context need to access app resource.
     * @return DarkSkyForecastResponse
     */
    public static DarkSkyForecastResponse getLocalStorageResponse(Context context) {
        DarkSkyForecastResponse response = null;
        Gson gson = new Gson();
        String source = readRawTextFile(context, R.raw.api_response);
        if(! TextUtils.isEmpty(source)) {
            response = gson.fromJson(source, DarkSkyForecastResponse.class);
        }
        return response;
    }

    /**
     * This method returns skycon View according to icon type description.
     *
     * @param activity - Activity Instance
     * @param iconType - string value which is a decider to provide skycon View instance
     *
     * @return SkyconView instance
     */
    public static SkyconView getSkyconView(Activity activity, String iconType) {
        SkyconView skyconView = null;
        switch (iconType) {
            case ForecastIconType.CLEAR_DAY:
                skyconView = new SunView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_clear_day),
                        activity.getResources().getColor(R.color.color_clear_day));
                break;
            case ForecastIconType.CLEAR_NIGHT:
                skyconView = new MoonView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_clear_night),
                        activity.getResources().getColor(R.color.color_clear_night));
                break;
            case ForecastIconType.CLOUDY:
                skyconView = new CloudView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_cloud),
                        activity.getResources().getColor(R.color.color_cloud));
                break;
            case ForecastIconType.FOG:
                skyconView = new CloudFogView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_fog),
                        activity.getResources().getColor(R.color.color_fog));
                break;
            case ForecastIconType.PARTIALLY_CLOUDY_DAY:
                skyconView = new CloudSunView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_partially_cloudy_day),
                        activity.getResources().getColor(R.color.color_partially_cloudy_day));
                break;
            case ForecastIconType.PARTIALLY_CLOUDY_NIGHT:
                skyconView = new CloudMoonView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_partially_cloudy_night),
                        activity.getResources().getColor(R.color.color_partially_cloudy_night));
                break;
            case ForecastIconType.RAIN:
                skyconView = new CloudRainView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_rain),
                        activity.getResources().getColor(R.color.color_rain));
                break;
            case ForecastIconType.SLEET:
                skyconView = new CloudSnowView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_snow),
                        activity.getResources().getColor(R.color.color_snow));
                break;
            case ForecastIconType.SNOW:
                skyconView = new CloudSnowView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_snow),
                        activity.getResources().getColor(R.color.color_snow));
                break;
            case ForecastIconType.WIND:
                skyconView = new WindView(activity,false,
                        true,
                        activity.getResources().getColor(R.color.color_line_wind),
                        activity.getResources().getColor(R.color.color_wind));
                break;
        }
        return skyconView;
    }

    /**
     * This method return formatted current time for a time zone.
     *
     * @param timeZone : String value of time zone
     *
     * @return formatted time text.
     */
    public static String getFormattedTime(String timeZone) {
        String result = null;
        if(timeZone != null) {
            TimeZone zone = TimeZone.getTimeZone(timeZone);
            SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.TIME_FORMAT);
            dateFormat.setTimeZone(zone);
            result = dateFormat.format(new Date());
        }
        return result;
    }

    /**
     * This method returns formatted date of input time and timezone.
     *
     * @param timeZone : String value of time zone
     * @param time : time in hours
     *
     * @return String
     */
    public static String getFormattedDate(String timeZone, int time) {
        String result = null;
        if(timeZone != null) {
            TimeZone zone = TimeZone.getTimeZone(timeZone);
            SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.SHORT_DATE_FORMAT);
            dateFormat.setTimeZone(zone);
            result = dateFormat.format(new Date(time * CommonConstants.TIME_ONE_HOUR_IN_MILLS));
        }
        return result;
    }

    /**
     * This method returns formatted short string of time of input time and timezone.
     *
     * @param timeZone : String value of time zone
     * @param time : time in hours
     *
     * @return String
     */
    public static String getFormattedShortStringTime(String timeZone, int time) {
        String result = null;
        if(timeZone != null) {
            TimeZone zone = TimeZone.getTimeZone(timeZone);
            SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.SHORT_TIME_FORMAT);
            dateFormat.setTimeZone(zone);
            result = dateFormat.format(new Date(time * CommonConstants.TIME_ONE_HOUR_IN_MILLS));
        }
        return result;
    }

    /**
     * This method checks whether Network is available or not and return true/false according to
     * the state.
     *
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        boolean result = false;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            result = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
        return result;
    }
}

