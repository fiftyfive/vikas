package com.darksky.poc.model.view;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.darksky.poc.model.LocationData;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.network.DarkSkyAPIManager;
import com.darksky.poc.storage.SharedPreferenceManager;

/**
 * This is a ViewModel class which is used to maintain MVVM design pattern(Model-View-ViewModel).
 * This class provide interface to fetch data from service and local storage.
 *
 * This class keeps properties into live observables : LiveData.
 */
public class LocationDataViewModel extends ViewModel implements
                Response.Listener<DarkSkyForecastResponse>, Response.ErrorListener {
    private MutableLiveData<LocationData> mLocationLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> mForecastResponseLiveData = new MutableLiveData<>();

    public MutableLiveData<LocationData> getLocationLiveData() {
        return mLocationLiveData;
    }

    public MutableLiveData<Object> getForecastResponseLiveData() {
        return mForecastResponseLiveData;
    }

    public void loadLocationDataFromPreference(Context context) {
        LocationData locationData =
                SharedPreferenceManager.getInstance(context).getLocationData();
        mLocationLiveData.setValue(locationData);
    }

    public void fetchForecastResponseForLocationData(Context context, LocationData locationData) {
        DarkSkyAPIManager.getDarkSkyForeCastResponse
                (context, this, this, locationData.getLatitude(),
                        locationData.getLongitude());
    }

    /**
     * This method store Location Data into Shared Preference
     *
     * @param context : Context
     * @param locationData : LocationData
     *
     * @return true/false if succesfully saved.
     */
    public boolean saveLocationDataIntoSharedPreference(Context context,
                                                         LocationData locationData) {
        // Store Location Properties
        return SharedPreferenceManager.getInstance(context).saveLocationData(locationData);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mForecastResponseLiveData.setValue(error);
    }

    @Override
    public void onResponse(DarkSkyForecastResponse response) {
        mForecastResponseLiveData.setValue(response);
    }
}
