package com.darksky.poc;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.darksky.poc.fragements.LocationUserInputFragment;
import com.darksky.poc.fragements.WeatherCurrentForecastFragment;
import com.darksky.poc.fragements.WeatherHoursForecastFragment;
import com.darksky.poc.model.LocationData;
import com.darksky.poc.model.view.LocationDataViewModel;

/**
 * Main Application Launcher Activity
 */
public class MainActivity extends AppCompatActivity implements
                                    LocationUserInputFragment.LocationSubmitCallback,
        WeatherCurrentForecastFragment.WeatherHoursForecastClickCallback {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Initialize Views
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * This method done initialization.
     */
    private void init() {
        // Create LocationViewDataModel instance.
        LocationDataViewModel viewModel = ViewModelProviders.of(this).
                get(LocationDataViewModel.class);
        // Add Observer on live data to listen the changes.
        viewModel.getLocationLiveData().observe(this, new Observer<LocationData>() {
            @Override
            public void onChanged(@Nullable LocationData o) {
                // On value change - update user interface.
                updateUserInterface(o);
            }
        });
        // Invoke view model to load persist LocationData.
        viewModel.loadLocationDataFromPreference(this.getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This method display user interface.
     *
     * @param locationData : LocationData
     */
    private void updateUserInterface(LocationData locationData) {
        // Fragment Manager Instance.
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        // Start Transaction
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // If location data is null then display LocationUserInputFragment screen.
        if(locationData == null) {
            LocationUserInputFragment locationUserInputFragment = new LocationUserInputFragment();
            // Set Callback
            locationUserInputFragment.setLocationSubmitCallback(this);
            // Add Fragment into the transaction.
            transaction.add(R.id.placeholder, locationUserInputFragment);
            transaction.commit();
        } else {
            // If location data is not null then display WeatherCurrentForecastFragment screen.
            WeatherCurrentForecastFragment forecastFragment = new WeatherCurrentForecastFragment();
            // Set Callback
            forecastFragment.setWeatherHoursForecastClickCallback(this);
            // Add Fragment into the transaction.
            transaction.add(R.id.placeholder, forecastFragment);
            transaction.commit();
        }
    }

    @Override
    public void onLocationSubmitEvent(LocationData locationData) {
        // On Successfully provided location data, display WeatherCurrentForecastFragment.
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        // Start Transaction
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        WeatherCurrentForecastFragment forecastFragment = WeatherCurrentForecastFragment.
                newInstance(locationData.getLatitude(), locationData.getLongitude());
        // Set Callback
        forecastFragment.setWeatherHoursForecastClickCallback(this);
        // Replace Fragment into the transaction.
        transaction.replace(R.id.placeholder, forecastFragment);
        transaction.commit();
    }

    @Override
    public void onWeatherHoursForecastClicked() {
        // On click related to weather hours based forecast, display WeatherHoursForecastFragment.
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        // Begin Transaction
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        WeatherHoursForecastFragment fragment = new WeatherHoursForecastFragment();
        // Add Fragment into the transaction.
        transaction.add(R.id.placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(! (getSupportFragmentManager().getBackStackEntryCount() > 1)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }
}
