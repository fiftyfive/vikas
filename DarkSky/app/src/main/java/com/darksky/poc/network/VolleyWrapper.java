package com.darksky.poc.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/*
   This class is a single volley network Class which provides a single entry to
   create Request Queue and to access it
 */
class VolleyWrapper {
    private static VolleyWrapper mVolleyWrapper;

    private Context mContext;
    private RequestQueue mRequestQueue;

    private VolleyWrapper(Context ctx) {
        mContext = ctx;
        mRequestQueue = getRequestQueue();
    }

    /**
     * This method creates(if required) and returns VolleyWrapper instance
     *
     * @param context - Context instance
     * @return VolleyWrapper instance
     */
     static synchronized VolleyWrapper getInstance(Context context) {
        if (mVolleyWrapper == null) {
            synchronized (VolleyWrapper.class) {
                if (mVolleyWrapper == null) {
                    mVolleyWrapper = new VolleyWrapper(context);
                }
                return mVolleyWrapper;
            }
        }
        return mVolleyWrapper;
    }

    /**
     * This method generate RequestQueue instance if not created earlier and returns it.
     *
     * @return RequestQueue
     */
    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * This method add Volley request to queue
     *
     * @param request - Request instance
     */
    <T> void addToRequestQueue(Request<T> request) {
        request.setShouldCache(false);
        mRequestQueue.add(request);
    }
}
