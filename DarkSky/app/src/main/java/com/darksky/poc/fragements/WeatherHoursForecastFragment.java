package com.darksky.poc.fragements;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.darksky.poc.R;
import com.darksky.poc.model.api.DarkSkyForecastResponse;
import com.darksky.poc.model.api.DatumHourly;
import com.darksky.poc.model.api.Hourly;
import com.darksky.poc.model.view.LocationDataViewModel;
import com.darksky.poc.util.AppUtil;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Arrays;
import java.util.List;

/**
 * This is a fragment class which is responsible to display temperature report hourly basis
 * in graph view.
 */
public class WeatherHoursForecastFragment  extends BaseFragment {
    // Properties Declarations
    // Views Object Declarations
    private XYPlot mPlot;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weather_hours_tempreture, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Initialization of views
        init(view);
    }

    /**
     * This method initialized fragment views
     *
     * @param view : Fragment View
     */
    private void init(View view) {
        showBackButton();
        // initialize our XYPlot reference:
        mPlot = view.findViewById(R.id.plot);
        LocationDataViewModel viewModel = ViewModelProviders.of(getActivity()).
                get(LocationDataViewModel.class);
        if(viewModel.getForecastResponseLiveData() != null) {
            Object obj = viewModel.getForecastResponseLiveData().getValue();
            if(obj instanceof DarkSkyForecastResponse) {
                drawGraph((DarkSkyForecastResponse) obj);
            }
        }
    }

    /**
     * Changes the icon of the drawer to back
     */
    public void showBackButton() {
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * This method set properties and data of graph.
     *
     * @param response DarkSkyForecastResponse
     */
    private void drawGraph(final DarkSkyForecastResponse response) {
        if(response != null && response.getHourly() != null) {
            mPlot.setVisibility(View.VISIBLE);
            setGraphSpecificProperties();
            setDomainSpecificProperties();
            setRangeSpecificProperties();
            setGraphRangeSeries(response);
            setLineLabelData(response);
        } else {
            showError(R.string.label_dialog_title,
                    R.string.label_dialog_current_weather_data_error);
        }
    }

    /**
     * This method sets graph specific properties.
     */
    private void setGraphSpecificProperties() {
        mPlot.getGraph().getGridBackgroundPaint().setColor
                (getColorFromResource(R.color.color_window_background));
        // set graph title color
        mPlot.getTitle().getLabelPaint().setColor(getColorFromResource(R.color.color_graph_body));
        mPlot.getTitle().getLabelPaint().setTextSize
                (getDimensionFromResource(R.dimen.app_body_text_size));
        mPlot.getTitle().getLabelPaint().setTypeface(Typeface.SANS_SERIF);
    }

    /**
     * This method sets graph domain specific properties.
     */
    private void setDomainSpecificProperties() {
        // set domain title color
        mPlot.getDomainTitle().getLabelPaint().setColor(getColorFromResource
                (R.color.color_graph_body));
        mPlot.getDomainTitle().getLabelPaint().setTypeface(Typeface.SANS_SERIF);
        // set domain specific color of graph
        mPlot.getGraph().getDomainGridLinePaint().
                setColor(getColorFromResource(R.color.color_graph_lines));
        mPlot.getGraph().getDomainGridLinePaint().setStrokeWidth
                (getDimensionFromResource(R.dimen.px_1));
        mPlot.getGraph().getDomainOriginLinePaint().setColor
                (getColorFromResource(R.color.color_graph_axis));
        mPlot.getGraph().getDomainSubGridLinePaint().setColor
                (getColorFromResource(R.color.color_graph_lines));
    }

    /**
     * This method sets graph range specific properties.
     */
    private void setRangeSpecificProperties() {
        // set range title color
        mPlot.getRangeTitle().getLabelPaint().setColor
                (getColorFromResource(R.color.color_graph_body));
        mPlot.getRangeTitle().getLabelPaint().setTypeface(Typeface.SANS_SERIF);
        mPlot.getRangeTitle().setText(getString(R.string.label_graph_temperature) +
                (char) 0x00B0 + getString(R.string.label_graph_temperature_append_c).toLowerCase());
        // set range specific color
        mPlot.getGraph().getRangeGridLinePaint().setColor
                (getColorFromResource(R.color.color_graph_lines));
        mPlot.getGraph().getRangeGridLinePaint().setStrokeWidth
                (getDimensionFromResource(R.dimen.px_1));
        mPlot.getGraph().getRangeOriginLinePaint().
                setColor(getColorFromResource(R.color.color_graph_axis));
        mPlot.getGraph().getRangeSubGridLinePaint().setColor
                (getColorFromResource(R.color.color_window_background));
    }

    /**
     * This method set range data.
     *
     * @param response : DarkSkyForecastResponse
     */
    private void setGraphRangeSeries(DarkSkyForecastResponse response) {
        // Get Range Data
        Number[] series1Numbers = getXYSeriesDataArray(response);
        // Turn the above arrays into XYSeries':
        // (Y_VALS_ONLY means use the element index as the x value)
        SimpleXYSeries series1 = new SimpleXYSeries(
                Arrays.asList(series1Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                getString(R.string.label_graph_temperature_hourly));
        // create formatters to use for drawing a series using LineAndPointRenderer
        // and configure them from xml:
        PointLabelFormatter formatter = new PointLabelFormatter
                (getColorFromResource(R.color.color_graph_point_label));
        formatter.getTextPaint().setTextSize
                (getDimensionFromResource(R.dimen.graph_data_body));
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(getColorFromResource(R.color.color_graph_slope),
                        getColorFromResource(R.color.color_graph_point),
                        getColorFromResource(R.color.color_graph_area), formatter);
        series1Format.getLinePaint().setStrokeWidth(getDimensionFromResource(R.dimen.dp_1));
        series1Format.getLinePaint().setTypeface(Typeface.SANS_SERIF);
        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(5,
                                    CatmullRomInterpolator.Type.Centripetal));
        // add a new series' to the xyplot:
        mPlot.addSeries(series1, series1Format);
    }

    /**
     * This method sets Line Label data and update paint style for this.
     *
     * @param response - DarkSkyForecastResponse
     */
    private void setLineLabelData(final DarkSkyForecastResponse response) {
        // Get Domain Data.
        final Number[] domainLabels = getDomainLabelsArray(response);
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).getPaint().
                setColor(getColorFromResource(R.color.color_graph_body));
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).getPaint().setColor
                (getColorFromResource(R.color.color_graph_body));
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).getPaint().
                setTextSize(getDimensionFromResource(R.dimen.graph_data_header));
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).getPaint().
                setTypeface(Typeface.SANS_SERIF);
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).getPaint().
                setTextSize(getDimensionFromResource(R.dimen.graph_data_header));
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).getPaint().
                setTypeface(Typeface.SANS_SERIF);
        mPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append
                        (getDateInStringFormat((int) domainLabels[i], response.getTimezone()) +
                                " - " + getTimeInStringFormat((int) domainLabels[i], response.getTimezone()));
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
    }

    /**
     * This method collects domain data for the graph from api response.
     *
     * @param response DarkSkyForecastResponse
     * @return Number[] (Domain Data)
     */
    private Number[] getDomainLabelsArray(DarkSkyForecastResponse response) {
        Number[] domainSeries = null;
        Hourly hourly = response.getHourly();
        if(hourly != null) {
            List<DatumHourly> data = hourly.getData();
            if(data != null && data.size() > 0) {
                domainSeries = new Integer[data.size()];
                int index = 0;
                for(DatumHourly hourlyData : data) {
                    domainSeries[index++] = hourlyData.getTime();
                }
            }
        }
        return domainSeries;
    }

    /**
     * This method collects range data for the graph from api response.
     *
     * @param response DarkSkyForecastResponse
     * @return Number[] (Range Data)
     */
    private Number[] getXYSeriesDataArray(DarkSkyForecastResponse response) {
        Number[] xySeries = null;
        Hourly hourly = response.getHourly();
        if(hourly != null) {
            List<DatumHourly> data = hourly.getData();
            if(data != null && data.size() > 0) {
                xySeries = new Double[data.size()];
                int index = 0;
                for(DatumHourly hourlyData : data) {
                    xySeries[index++] = hourlyData.getTemperature();
                }
            }
        }
        return xySeries;
    }

    /**
     * This method return formatted date for a input zone.
     *
     * @param time (in second)
     * @param zone (time zone)
     *
     * @return formatted date string
     */
    private String getDateInStringFormat(int time, String zone) {
        return AppUtil.getFormattedDate(zone, time);
    }

    /**
     * This method return formatted time for a input zone.
     *
     * @param time (in second)
     * @param zone (time zone)
     *
     * @return formatted time string
     */
    private String getTimeInStringFormat(int time, String zone) {
        return AppUtil.getFormattedShortStringTime(zone, time);
    }
}
