package com.darksky.poc.util.constants;

/**
 * This interface defines Network Layer Specific Constants.
 */
public interface NetworkConstants {
    String DARK_SKY_BASE_URL = "https://api.darksky.net";
    String DARK_SKY_API_KEY = "59760e9fef2bf158db5d30ab7990d4e5";

    String DARK_SKY_API_PATH_SEPARATOR = "/";
    String DARK_SKY_API_COMMA_SEPARATOR = ",";
    String DARK_SKY_API_EQUALS = "=";
    String DARK_SKY_API_QUESTION_MARK = "?";

    String DARK_SKY_SERVICE_FORECAST = "forecast";
    String DARK_SKY_SERVICE_FORECAST_PARAM_LATITUDE = "latitude";
    String DARK_SKY_SERVICE_FORECAST_PARAM_LONGITUDE = "longitude";
    String DARK_SKY_SERVICE_FORECAST_PARAM_UNITS = "units";
    String DARK_SKY_SERVICE_FORECAST_PARAM_UNITS_VALUE = "si";
}
