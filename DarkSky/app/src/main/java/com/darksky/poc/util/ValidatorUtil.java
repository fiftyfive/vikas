package com.darksky.poc.util;

import android.text.TextUtils;
import android.util.Log;

/**
 * This is utility for data validation.
 */
public class ValidatorUtil {
    private static final String TAG = "ValidatorUtil";

    /**
     * This method return true if provided longitude string is valid longitude value.
     *
     * @param longitude : String
     * @return : boolean value
     */
    public static boolean isValidLongitude(String longitude) {
        return isValidDoubleString(longitude);
    }

    /**
     * This method return true if provided latitude string is valid latitude value.
     *
     * @param latitude : String
     * @return : boolean value
     */
    public static boolean isValidLatitude(String latitude) {
        return isValidDoubleString(latitude);
    }

    /**
     * This method returns true if input string could be parsed into double type.
     *
     * @param data : string data
     * @return - boolean value
     */
    private static boolean isValidDoubleString(String data) {
        boolean result = false;
        if(! TextUtils.isEmpty(data)) {
            try {
                Double.parseDouble(data);
                result = true;
            } catch (NumberFormatException exception) {
                Log.e(TAG, exception.getMessage());
            }
        }
        return result;
    }
}
